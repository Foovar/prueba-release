## [1.11.1](https://gitlab.com/Foovar/prueba-release/compare/1.11.0...1.11.1) (2021-06-26)

# [1.11.0](https://gitlab.com/Foovar/prueba-release/compare/1.10.1...1.11.0) (2021-06-26)


### Bug Fixes

* Solo un fix 2 xd ([36039f2](https://gitlab.com/Foovar/prueba-release/commit/36039f24b6f5ee696a2200920261601677d04f16))
* Solo un fix 3 xd ([255b58a](https://gitlab.com/Foovar/prueba-release/commit/255b58acee2049ca600902f2c0a3dfa4c95a4a0b))


### Features

* Solo feat 1 xd ([07e0d03](https://gitlab.com/Foovar/prueba-release/commit/07e0d032969b0320c8f55ad32bfd0918be992c06))

## [1.10.1](https://gitlab.com/Foovar/prueba-release/compare/1.10.0...1.10.1) (2021-06-26)


### Bug Fixes

* Solo un fix xd ([dc6ecc9](https://gitlab.com/Foovar/prueba-release/commit/dc6ecc99e78a07d86729b9fcb16f1c75d2b0f45d))

# [1.10.0](https://gitlab.com/Foovar/prueba-release/compare/1.9.3...1.10.0) (2021-06-26)


### Features

* Nueva feature ([f8ff288](https://gitlab.com/Foovar/prueba-release/commit/f8ff2889d5c6686e669f873619a54aaef20cce40))

## 1.9.3 (2021-06-26)

## 1.9.2 (2021-06-26)

## 1.9.1 (2021-06-26)

## 1.9.1 (2021-06-26)

